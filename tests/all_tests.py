import base_test, time


class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
        # Step 1: Click on Login and the login page loads
        assert self.welcomepage.click_login(self.loginpage)

        # Step 2: Login to the web page and confirm the main page appears
        assert self.loginpage.login(self.mainpage, self.param.username, self.param.password)

        # Step 3: Logout of the web page and check the welcome page appears
        assert self.mainpage.logout(self.welcomepage)

    def test_02_find_and_send_text_multiple_elements(self):
        # Step 1: Click on Login and the login page loads
        assert self.welcomepage.click_login(self.loginpage)

        # Step 2: Enter some text inot the username and password fields
        assert self.loginpage.multi_page_element("test")

        # Step 3: Click the home page and check it redirects
        assert self.loginpage.click_home(self.welcomepage)

    def test_03_find_and_send_text_to_pop_up(self):
        # Step 1: Load login page
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Login to the portal
        assert self.loginpage.login(self.mainpage, self.param.username, self.param.password)
        # Step 3: Click on the say somethin button and enter some text.
        assert self.mainpage.click_and_say_something("test")
        # Step 4: Logout
        assert self.mainpage.logout(self.welcomepage)

    def test_04_test_frames(self):
        # Step 1: Load login pge
        assert self.welcomepage.click_login(self.loginpage)
        # Step 2: Login to the portal
        assert self.loginpage.login(self.mainpage, self.param.username, self.param.password)
        # Step 3: Open frames page
        assert self.mainpage.open_frames_window(self.framespage)
        # Step 4:
        assert self.framespage.switch_to_main_frame(self.)


