from selenium import webdriver
import yaml
from selenium.webdriver import DesiredCapabilities


class Parameters():
    def __init__(self):
        with open("../settings.yaml") as stream:
            try:
                settings = yaml.load(stream)
            except yaml.YAMLError as exc:
                print(exc)

        self.w = webdriver.Chrome()
        # self.w = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub',
        #                          desired_capabilities=DesiredCapabilities.HTMLUNITWITHJS)
        self.rootUrl = settings["root_url"]
        self.w.implicitly_wait(settings["wait_time"])
        self.username = settings["username"]
        self.password = settings["password"]
