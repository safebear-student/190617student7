from page_objects import PageObject, PageElement


class MainFramePage(PageObject):
    safebear_image = PageElement(xpath='//img[@src="/static/safebear-logo.png"]')

    def isMainFrame(self):
        return self.safebear_image.is_displayed()
