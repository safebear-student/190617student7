from page_objects import PageObject, PageElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class MainPage(PageObject):

    logout_link = PageElement(link_text="Logout")
    say_something_button = PageElement(xpath='//button[text()="Say Something"]')
    say_something_text_area = PageElement(id_ = "saySomething")
    go_to_frames_button = PageElement(link_text="Go to the frames »")

    current_page = None

    def check_page(self):
        return "Logged" in self.w.title

    def logout(self, welcomepage):
        self.logout_link.click()
        return welcomepage.check_page()

    def click_and_say_something(self, text):
        self.say_something_button.click()
        try:
            element = WebDriverWait(self.w, 10).until(EC.alert_is_present())
        except TimeoutError:
            print("Waited 10 seconds and pop-up didnt appear")

        alert = self.w.switch_to_alert()
        alert.send_keys(text)
        alert.accept()
        return text in self.say_something_text_area.text

    def open_frames_window(self, framespage):
        self.go_to_frames_button.click()
        self.current_page = self.w.current_window_handle
        self.w.switch_to_window(self.w.window_handles[len(self.w.window_handles )-1])

        return framespage.check_page()

