from page_objects import PageObject, PageElement, MultiPageElement


class LoginPage(PageObject):
    username_field = PageElement(id_="myid")
    password_field = PageElement(id_="mypass")
    all_fields = MultiPageElement(xpath='//input[@type="text"]')
    home_button = PageElement(link_text="Home")

    def check_page(self):
        return "Sign" in self.w.title

    def login(self, mainpage, username, password):
        self.username_field.send_keys(username)
        self.password_field.send_keys(password)
        self.password_field.submit()
        return mainpage.check_page()

    def multi_page_element(self, text):
        self.all_fields = text
        return text in self.username_field.get_attribute("value") and text in self.password_field.get_attribute("value")

    def click_home(self, welcomepage):
        self.home_button.click()
        return welcomepage.check_page()
